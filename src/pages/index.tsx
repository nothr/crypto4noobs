import { type NextPage } from "next";
import Head from "next/head";
import Link from "next/link";

const Home: NextPage = () => {
  return (
    <>
      <Head>
        <title>Crypto4Noobs | Cooming Soon</title>
        <meta name="description" content="A website for basic crypto development" />
      </Head>
      <main className="flex min-h-screen flex-col items-center justify-center bg-gradient-to-b from-[#2e026d] to-[#15162c]">
        <div className="container flex flex-col items-center justify-center gap-12 px-4 py-16 text-center ">
          <h1 className="text-5xl font-extrabold tracking-tight text-white sm:text-[5rem]">
            Crypto for <span className="text-[hsl(280,100%,70%)]">Noobs</span>
          </h1>
          <div className="text-xl font-semibold  text-white bg-orange-400 p-3 px-8 rounded-xl">
            Comming soon!
          </div>
        </div>
      </main>
    </>
  );
};

export default Home;
